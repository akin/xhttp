
#include "xclient.h"
#include <iostream>

namespace xhttp {

Client::Client()
{
}

Client::~Client()
{
}

void Client::onConnection(const ConnectionCallback& callback)
{
	m_connectioncb = callback;
}

void Client::setStatus(Status status)
{
	m_status = status;
	if (m_connectioncb)
	{
		static const std::string empty = "";
		m_connectioncb(status, empty);
	}
}

void Client::setStatus(Status status, const std::string& msg)
{
	if(status == m_status)
	{
		return;
	}
	m_status = status;
	if (m_connectioncb)
	{
		m_connectioncb(status, msg);
	}
}

void Client::onCommunicationError(const std::string& msg)
{
	std::cout << "Communication error !! " << msg << std::endl;
}

Status Client::getStatus() const
{
	return m_status;
}

} // NS xhttp
