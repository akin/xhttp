
#include "xrequest.h"
#include "xcommon.h"

namespace xhttp {

void Request::onResponse(const ResponseCallback& callback)
{
    m_onResponse = callback;
}

void Request::responseData(RequestStatus status, const char* data, size_t size) const
{
    if(m_onResponse)
    {
        m_onResponse(status, data, size);
    }
}

void Request::setAdditionalHeader(const std::string& key, const std::string& value)
{
    std::string tmp{key};
    toLower(tmp);
    
    m_headers[tmp] = value;
}

bool Request::getAdditionalHeader(const std::string& key , std::string& value) const
{
    std::string tmp{key};
    toLower(tmp);
    
    auto iter = m_headers.find(tmp);
    if(iter == m_headers.end())
    {
        return false;
    }
    value = iter->second;
    return true;
}

void Request::setData(const std::string& contentType, const void *data, size_t size)
{
    m_contentType = contentType;
    m_data = data;
    m_size = size;
}

void Request::setData(const std::string& contentType, DataCallback& dataCallback, size_t size)
{
    m_contentType = contentType;
    m_data = nullptr;
    m_size = size;
    m_dataCallback = dataCallback;
}

} // NS xhttp
