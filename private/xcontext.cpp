#include "xcontext.h"
#include <asio.hpp>
#include <asio/ssl.hpp>
#include <asio/ssl/context_base.hpp>
#include <asio/ssl/context.hpp>

#include "xclient_http.h"
#include "xclient_https.h"

#include <atomic>
#include <condition_variable>
#include <mutex>
#include <thread>

#include <assert.h>

namespace xhttp {

class Context::Implementation
{
private:
    asio::io_context m_context;
    asio::ssl::context m_secure;

    std::vector<std::thread> m_threads;
    std::atomic_bool m_running = false;

    mutable std::mutex m_mutex;

    size_t m_threadCount = 1;
public:
    Implementation()
    : m_secure{asio::ssl::context::sslv23} // apparently google uses V3.
    , m_running{false}
    {
        m_secure.set_options(asio::ssl::context::default_workarounds);
    }

    ~Implementation()
    {
        stop();
    }

    void setThreadCount(size_t count)
    {
        // TODO, Change amount of threads while running..
        assert(count < 1);
        m_threadCount = count;
    }

    size_t setThreadCount() const
    {
        return m_threadCount;
    }
    
    bool loadCertificate(const std::string& path)
    {
        // SSL
        asio::error_code error;
        m_secure.load_verify_file(path, error);
        
        if(error)
        {
            std::string message = error.message();
            assert(false);
            return false;
        }
        return true;
    }

    std::shared_ptr<Client> create(ConnectionType type, const std::string& server)
    {
        switch(type)
        {
        case ConnectionType::http :
            return std::shared_ptr<Client>(new ClientHTTP(m_context, server));
        case ConnectionType::https :
            return std::shared_ptr<Client>(new ClientHTTPS(m_context, m_secure, server));
        default:
            break;
        }
        assert(false);
        return nullptr;
    }

    void run()
    {
        std::unique_lock<std::mutex> lock{m_mutex};
        if(!m_running)
        {
            m_running = true;
            m_context.restart();

            for(size_t i = 0 ; i < m_threadCount ; ++i)
            {
                m_threads.emplace_back([this](){
                    while(m_running)
                    {
                        auto count = m_context.run();

                        // do not burn cores, if theres nothing to do!
                        // context will return, if theres things to do.
                        if(count == 0 && m_running)
                        {
							const std::chrono::milliseconds sleepytime(100);
							std::this_thread::sleep_for(sleepytime);
                        }
                    }
                });
            }
        }
    }

    void stop()
    {
        std::unique_lock<std::mutex> lock{mutex};
        m_running = false;
        m_context.stop();

        for(auto& thread : m_threads)
        {
            thread.join();
        }
        m_threads.clear();
    }
};

Context::Context()
: m_implemenation{new Implementation}
{
}

Context::~Context()
{
}

bool Context::loadCertificate(const std::string& path)
{
    return m_implemenation->loadCertificate(path);
}

void Context::setThreadCount(size_t count)
{
    m_implemenation->setThreadCount(count);
}

size_t Context::getThreadCount() const
{
    return m_implemenation->getThreadCount();
}

void Context::run()
{
	m_implemenation->run();
}

void Context::stop()
{
	m_implemenation->stop();
}

std::shared_ptr<Client> Context::create(ConnectionType type, const std::string& server)
{
	return m_implemenation->create(type, server);
}

} // ns xhttp
