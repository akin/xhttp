
#ifndef XCLIENT_HTTP_H_
#define XCLIENT_HTTP_H_

#include "xhttp.h"
#include <iostream>
#include <istream>
#include <ostream>
#include <string>
#include <memory>
#include "xhttpparser.h"
#include "xclient_base.h"

namespace xhttp {

class ClientHTTP : public ClientBase<tcp::socket>
{
private:
	tcp::socket m_socket;
public:
	ClientHTTP(asio::io_context& io_context, const std::string& server)
	: ClientBase<tcp::socket>(io_context, m_socket, server)
	, m_socket(io_context)
	{
	}

	virtual ~ClientHTTP()
	{
	}
    
    void open() final
    {
        setStatus(Status::connecting);
        
        m_resolver.async_resolve(m_server, "http", [this](const asio::error_code& error, const tcp::resolver::results_type& endpoints)
		{
			if(error)
			{
				setStatus(Status::error, error.message());
				return;
			}
			
			// Attempt a connection to each endpoint in the list until we
			// successfully establish a connection.
			asio::async_connect(m_socket, endpoints, [this](const asio::error_code& error, const asio::ip::tcp::endpoint& endpoint)
			{
				if(error)
				{
					setStatus(Status::error, error.message());
					return;
				}
				setStatus(Status::connected);
			});
		});
    }
    
    void close() final
    {
        setStatus(Status::disconnected);
        m_socket.close();
    }
    
    bool isOpen() final
    {
        return m_socket.is_open();
    }
};

} // NS xhttp

#endif // XCLIENT_HTTP_H_
