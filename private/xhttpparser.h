
#ifndef XHTTPPARSER_H_
#define XHTTPPARSER_H_

#include "xhttp.h"
#include <string>
#include <vector>
#include <ostream>
#include <unordered_map>
#include <algorithm>
#include "xrequest.h"
#include "xcommon.h"

namespace xhttp {

class HTTPParser
{
private:
    size_t findLine(const char *data, size_t iter, size_t max) const
    {
        // rn
        ++iter;
        for(; iter < max ; ++iter)
        {
            if(data[iter] == '\n' && data[iter - 1] == '\r')
            {
                return iter + 1;
            }
        }
        return max;
    }
    size_t findBody(const char *data, size_t iter, size_t max) const
    {
        // rnrn
        // 321^
        iter += 4;
        for(; iter < max ; ++iter)
        {
            if(data[iter] == '\n' && data[iter - 1] == '\r' && data[iter - 2] == '\n' && data[iter - 3] == '\r')
            {
                return iter + 1;
            }
        }
        return max;
    }
    size_t findChar(char c, const char *data, size_t iter, size_t max) const
    {
        for(; iter < max ; ++iter)
        {
            if(data[iter] == c)
            {
                return iter;
            }
        }
        return max;
    }
    size_t findCharNot(char c, const char *data, size_t iter, size_t max) const
    {
        for(; iter < max ; ++iter)
        {
            if(data[iter] != c)
            {
                return iter;
            }
        }
        return max;
    }
private:
    std::vector<char> m_data;
    std::unordered_map<std::string, std::string> m_header;

	enum class State
	{
		HEADER,
		BODY
	};

    State m_state = State::HEADER;
    
    std::string m_protocol;
    std::string m_protocolVersion;
    int m_code;
    int m_remaining;
    std::string m_response;
private:
    void addHeader(const char* data, size_t size)
    {
        //CaseInSensitive:<optional whitespace><app defined value>
        //Content-Type: text/plain
        //Accept-Ranges: bytes
        //ETag: "1820025731"
        //Last-Modified: Sat, 30 Jun 2018 07:42:45 GMT
        //Content-Length: 5
        //Connection: close
        size_t sep = findChar(':', data, 0, size);
        if(sep >= size) return;
        
        std::string key{data , data + sep};
        toLower(key);
        
        size_t iter = findCharNot(' ', data, sep + 1, size);
        if(iter >= size) return;
        
        std::string value{data + iter , data + size};
        
        m_header[key] = value;
    }

    bool setStatus(const char* data, size_t size)
    {
        //HTTP/1.1 200 OK
        size_t blank = findChar(' ', data, 0, size);
        if(blank >= size) return false;
        size_t blank2 = findChar(' ', data, blank + 1, size);
        if(blank2 >= size) return false;
        
        size_t div = findChar('/', data, 0, blank);
        if(div >= blank) return false;
        m_protocol = std::string{data, data + div};
        m_protocolVersion = std::string{data +(div + 1), data + blank};
        
        std::string code = std::string{data + (blank + 1), data + blank2};
        m_code = std::stoi(code);
        
        m_response = std::string{data + blank2 + 1, data + (size)};
        
        return true;
    }
    
    int getIntHeader(const std::string& key, int value = 0)
    {
        auto it = m_header.find(key);
        if(it != m_header.end())
        {
            return std::stoi(it->second);
        }
        return value;
    }
public:
	HTTPParser()
	{
		m_data.reserve(512);
        m_header.reserve(32);
	}

	std::string getType()
	{
		return "http";
	}
    
    std::string getVersion() const
    {
        return "HTTP/1.1";
    }

	void createRequest(std::ostream& request_stream, const std::string& server, const Request& config)
	{
		request_stream << toString(config.type) << " " << config.path << " " << getVersion() << "\r\n";
		request_stream << "Host: " << server << "\r\n";

		// Generic headers!
		if(config.keepAlive == 0)
		{
			request_stream << "Connection: close\r\n";
		}
		else
		{
			request_stream << "Connection: keep-alive\r\n";
			request_stream << "Keep-Alive: timeout=" << config.keepAlive << ", max=1000\r\n";
		}
		if(!config.accept.empty())
		{
			request_stream << "Accept: " << config.accept << "\r\n";
		}
		else
		{
			request_stream << "Accept: */*\r\n";
		}
		if(!config.userAgent.empty())
		{
			request_stream << "User-Agent: " << config.userAgent << "\r\n";
		}

		// Send data headers!
		{
			if(!config.m_contentType.empty())
			{
				// Content-Type: text/html; charset=utf-8
				request_stream << "Content-Type: " << config.m_contentType << "\r\n";
			}
		}
        request_stream << "\r\n";
	}

	void append(const char* data, size_t iter, size_t size)
	{
		size_t at = m_data.size();
		m_data.resize(at + size);

		memcpy(&m_data[at], &data[iter], size);
	}
    
    void resetHeaders()
    {
        m_header.clear();
        
        m_protocol = "http";
        m_protocolVersion = "";
        m_code = 0;
        m_response = "";
        m_remaining = 0;
    }
    
    void processHeaders()
    {
        resetHeaders();
        
        // -rnrn
        if(m_data.size() < 4)
        {
            return;
        }
        
        // -rn
        size_t max = m_data.size() - 2;
        size_t iter = 0;
        {
            iter = findLine(m_data.data(), 0, max);
            setStatus(m_data.data(), iter - 2);
        }
        for(; iter < max ;)
        {
            size_t at = findLine(m_data.data(), iter, max);
            if(at == max)
            {
                break;
            }
            
            addHeader(&m_data[iter], (at - 2) - iter);
            iter = at;
        }
    }

    template <class Consumer>
	void response(RequestStatus status, Consumer& consumer, const char* data, size_t size)
	{
        size_t iter = 0;
        if(m_state == State::HEADER)
        {
            // First line is status
            // Each line contains one header
            // First blank line /r/n/r/n separates body.
            size_t end = findBody(data, iter, size);
            append(data, iter, end);
            
            iter = end;
            // Header continues?
            if(end == size)
            {
                return;
            }

            // Optional body!
            m_state = State::BODY;
            processHeaders();
            
            m_remaining = getIntHeader("content-length");
        }
        
        if(status == RequestStatus::completed && size == 0)
        {
            consumer.responseData(status, nullptr, 0);
            m_state = State::HEADER;
            return;
        }
        
        size_t consume = size - iter;
        m_remaining -= consume;
        consumer.responseData(status, data + iter, consume);
    }
};

} // xhttp

#endif // XHTTPPARSER_H_
