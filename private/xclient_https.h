
#ifndef XCLIENT_HTTPS_H_
#define XCLIENT_HTTPS_H_

#include "xclient_base.h"
#include <asio/ssl.hpp>

namespace xhttp {

enum { max_length = 1024 };

class ClientHTTPS : public ClientBase<asio::ssl::stream<asio::ip::tcp::socket>>
{
private:
    asio::ssl::stream<asio::ip::tcp::socket> m_socket;
public:
	ClientHTTPS(asio::io_context& io_context, asio::ssl::context& secure_context, const std::string& server)
	: ClientBase<asio::ssl::stream<asio::ip::tcp::socket>>(io_context, m_socket, server)
	, m_socket(io_context, secure_context)
	{
		m_socket.set_verify_mode(asio::ssl::verify_peer);
    	m_socket.set_verify_callback([this](bool preverified, asio::ssl::verify_context& ctx){
			// The verify callback can be used to check whether the certificate that is
			// being presented is valid for the peer. For example, RFC 2818 describes
			// the steps involved in doing this for HTTPS. Consult the OpenSSL
			// documentation for more details. Note that the callback is called once
			// for each certificate in the certificate chain, starting from the root
			// certificate authority.

			// In this example we will simply print the certificate's subject name.
			char subject_name[256];
			X509* cert = X509_STORE_CTX_get_current_cert(ctx.native_handle());
			X509_NAME_oneline(X509_get_subject_name(cert), subject_name, 256);
			std::cout << "Verifying " << subject_name << "\n";

			return preverified;
		});
	}

	virtual ~ClientHTTPS() 
	{
	}
    
    void open() final
    {
        setStatus(Status::connecting);
        
        m_resolver.async_resolve(m_server, "http", [this](const asio::error_code& error, const tcp::resolver::results_type& endpoints)
		{
			if(error)
			{
				setStatus(Status::error, error.message());
				return;
			}
			
			// Attempt a connection to each endpoint in the list until we
			// successfully establish a connection.
			asio::async_connect(m_socket.lowest_layer(), endpoints, [this](const asio::error_code& error, const asio::ip::tcp::endpoint& endpoint)
			{
				if(error)
				{
					setStatus(Status::error, error.message());
					return;
				}
				m_socket.async_handshake(asio::ssl::stream_base::client, [this](const asio::error_code& error)
                {
					if(error)
					{
						setStatus(Status::error, error.message());
						return;
					}
					setStatus(Status::connected);
                });
			});
		});
    }
    
    void close() final
    {
        setStatus(Status::disconnected);
        // lol, consistency..
        m_socket.shutdown();
        m_socket.lowest_layer().close();
    }
    
    bool isOpen() final
    {
        return m_socket.lowest_layer().is_open();
    }
};

} // NS xhttp

#endif // XCLIENT_HTTPS_H_
