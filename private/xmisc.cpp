
#include "xmisc.h"

namespace xhttp {

const char *toString(Status status)
{
	switch (status)
	{
	case xhttp::Status::error:
		return "error";
	case xhttp::Status::success:
		return "success";
	case xhttp::Status::connecting:
		return "connecting";
	case xhttp::Status::connected:
		return "connected";
	case xhttp::Status::disconnected:
		return "disconnected";
	default:
		break;
	}
	return nullptr;
}


const char *toString(RequestType type)
{
	switch(type)
	{
	case RequestType::get:
		return "GET";
	case RequestType::post:
		return "POST";
	case RequestType::put:
		return "PUT";
	case RequestType::del:
		return "DELETE";
	default:
		break;
	}
	return nullptr;
}

} // NS xhttp
