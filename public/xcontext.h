
#ifndef XHTTP_CONTEXT_H_
#define XHTTP_CONTEXT_H_

#include <memory>
#include <string>
#include "xmisc.h"

namespace xhttp {

class Client;

class Context
{
private:
	class Implementation;
	std::unique_ptr<Implementation> m_implemenation;
public:
	Context();
	~Context();
    
	// if(!ctx->loadCertificate("ca.pem")) { //nooo
	bool loadCertificate(const std::string& path);

	void setThreadCount(size_t count);
	size_t getThreadCount() const;

	void run();
	void stop();

	std::shared_ptr<Client> create(ConnectionType type, const std::string& server);
};

} // ns xhttp

#endif // XHTTP_CONTEXT_H_

