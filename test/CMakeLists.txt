cmake_minimum_required(VERSION 3.7)
option(TESTS_xhttp "Add XHTTP Tests" OFF)
if(TESTS_xhttp)
	project(xhttp-tests C CXX)
	add_executable(${PROJECT_NAME})

	file(GLOB CURRENT_SOURCES 
		${CMAKE_CURRENT_LIST_DIR}/*.cpp 
	)
	target_sources(
		${PROJECT_NAME} 
		PRIVATE 
			${CURRENT_SOURCES}
		)

	target_link_libraries(
		${TARGET_NAME} 
		PRIVATE
			catch
			xhttp
	)
endif()

