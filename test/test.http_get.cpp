
#ifndef XHTTP_GET_H_
#define XHTTP_GET_H_

#include <iostream>
#include "catch.hpp"
#include <xhttp.h>
#include <future>

TEST_CASE( "Simple HTTP GET client call.", "[HTTP GET]" ) {
	xhttp::Context context;
    
    std::string server = "www.icegem.net";
    server = "www.google.fi";
    auto connection = context.create(xhttp::https, server);

    SECTION( "First call" ) {
        {
            std::promise<xhttp::Status> status;
            auto connectionFuture = status.get_future();
            connection->onConnection([&status](xhttp::Status currentStatus, const std::string& msg){
                std::cout << "Connection " << xhttp::toString(currentStatus) << " " << msg << std::endl;
                
                if(currentStatus == xhttp::Status::connecting)
                    return;
                
                status.set_value(currentStatus);
            });
            connection->open();
            context.run();
            REQUIRE( connectionFuture.get() == xhttp::Status::connected );
            
            connection->onConnection(nullptr);
        }
        
        {
            std::promise<std::string> response;
            xhttp::Request request;
            request.type = xhttp::RequestType::get;
            request.path = "/connection";
            request.keepAlive = 0;
            
            request.onResponse([&response](xhttp::RequestStatus status, const void *data, size_t size) {
                if(size > 0 )
                {
                    std::string msg;
                    msg.resize(size);
                    std::memcpy(&msg[0], data, size);
                    
                    response.set_value(msg);
                }
            });
            REQUIRE( connection->getStatus() == xhttp::Status::connected );
            REQUIRE( connection->isOpen() );
            
            connection->send(request);
            context.run();
            
            auto content = response.get_future().get();
            
            REQUIRE( content.size() == 4 );
            REQUIRE( content == "true" );
        }
        
        connection->close();
        REQUIRE( !connection->isOpen() );
    }
    
    SECTION( "Second call" ) {
        {
            std::promise<xhttp::Status> status;
            auto connectionFuture = status.get_future();
            connection->onConnection([&status](xhttp::Status currentStatus, const std::string& msg){
                std::cout << "Connection " << xhttp::toString(currentStatus) << " " << msg << std::endl;
                
                if(currentStatus == xhttp::Status::connecting)
                    return;
                
                status.set_value(currentStatus);
            });
            connection->open();
            context.run();
            REQUIRE( connectionFuture.get() == xhttp::Status::connected );
            
            connection->onConnection(nullptr);
        }
        
        {
            std::promise<std::string> response;
            xhttp::Request request;
            request.type = xhttp::RequestType::get;
            request.path = "/connection";
            request.keepAlive = 0;
            
            request.onResponse([&response](xhttp::RequestStatus status, const void *data, size_t size) {
                if(size > 0 )
                {
                    std::string msg;
                    msg.resize(size);
                    std::memcpy(&msg[0], data, size);
                    
                    response.set_value(msg);
                }
            });
            REQUIRE( connection->getStatus() == xhttp::Status::connected );
            REQUIRE( connection->isOpen() );
            
            connection->send(request);
            context.run();
            
            auto content = response.get_future().get();
            
            REQUIRE( content.size() == 4 );
            REQUIRE( content == "true" );
        }
        
        connection->close();
        REQUIRE( !connection->isOpen() );
    }
    
    SECTION( "Third call keep alive test" ) {
        {
            std::promise<xhttp::Status> status;
            auto connectionFuture = status.get_future();
            connection->onConnection([&status](xhttp::Status currentStatus, const std::string& msg){
                std::cout << "Connection " << xhttp::toString(currentStatus) << " " << msg << std::endl;
                
                if(currentStatus == xhttp::Status::connecting)
                    return;
                
                status.set_value(currentStatus);
            });
            connection->open();
            context.run();
            REQUIRE( connectionFuture.get() == xhttp::Status::connected );
            
            connection->onConnection(nullptr);
        }
        
        xhttp::Request request;
        request.type = xhttp::RequestType::get;
        request.path = "/connection";
        request.keepAlive = 10;
        {
            std::promise<std::string> response;
            request.onResponse([&response](xhttp::RequestStatus status, const void *data, size_t size) {
                if(size > 0 )
                {
                    std::string msg;
                    msg.resize(size);
                    std::memcpy(&msg[0], data, size);
                    
                    response.set_value(msg);
                }
            });
            REQUIRE( connection->getStatus() == xhttp::Status::connected );
            REQUIRE( connection->isOpen() );
            
            connection->send(request);
            context.run();
            
            auto content = response.get_future().get();
            
            REQUIRE( content.size() == 4 );
            REQUIRE( content == "true" );
        }
        
        {
            std::promise<std::string> response;
            request.onResponse([&response](xhttp::RequestStatus status, const void *data, size_t size) {
                if(size > 0 )
                {
                    std::string msg;
                    msg.resize(size);
                    std::memcpy(&msg[0], data, size);
                    
                    response.set_value(msg);
                }
            });
            REQUIRE( connection->getStatus() == xhttp::Status::connected );
            REQUIRE( connection->isOpen() );
            
            connection->send(request);
            context.run();
            
            auto content = response.get_future().get();
            
            REQUIRE( content.size() == 4 );
            REQUIRE( content == "true" );
        }
    }
}

#endif
